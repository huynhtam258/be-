﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace coffeeapp.DTO
{
    public class BillOutDetailDTO
    {
        public int BillOutId { set; get; }
        //public string ProductName { set; get; }
        public int QuantityProduct { set; get; }
        public double? BillPriceTotal { set; get; }
    }
}