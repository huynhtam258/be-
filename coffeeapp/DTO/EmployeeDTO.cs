﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace coffeeapp.DTO
{
    public class EmployeeDTO
    {
        public string UserName { set; get; }
        public string Avartar { set; get; }
        public string PhoneNumber { set; get; }
        public string Email { set; get; }
        public string Password { set; get; }
    }
}