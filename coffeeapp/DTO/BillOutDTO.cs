﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace coffeeapp.DTO
{
    public class BillOutDTO
    {
        public int BillOutId { set; get; }
        public int EmployeeId { set; get; }
        public DateTime BillCreateDate { set; get; }
        public double BillPriceTotal { set; get; }
        public int AreaId { set; get; }
        public int NewId { set; get; }
    }       
}