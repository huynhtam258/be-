﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace coffeeapp.DTO
{
    public class TypeProductDTO
    {
        public int TypeProductId { set; get; }
        public string TypeProductName { set; get; }
        public string TypeProductIcon { set; get; }
    }
}