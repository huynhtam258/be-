﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace coffeeapp.DTO
{
    public class ProductDTO
    {
        public int ProductId { set; get; }
        public string ProductName { set; get; }
        public string ProductInfo { set; get; }
        public string ProductImage { set; get; }
        public double ProductPrice { set; get; }
        public int TypeProductId { set; get; }
    }
}