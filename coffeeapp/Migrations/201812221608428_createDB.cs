namespace coffeeapp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Areas",
                c => new
                    {
                        AreaId = c.Int(nullable: false, identity: true),
                        AreaName = c.String(nullable: false, maxLength: 50),
                        AreaAdress = c.String(maxLength: 100),
                        AreaPhone = c.String(maxLength: 15),
                    })
                .PrimaryKey(t => t.AreaId);
            
            CreateTable(
                "dbo.BillOuts",
                c => new
                    {
                        BillOutId = c.Int(nullable: false, identity: true),
                        EmployeeId = c.Int(),
                        CustomerPhone = c.String(maxLength: 15),
                        BillCreateDate = c.DateTime(nullable: false),
                        BillPriceTotal = c.Double(),
                        PaymentMethod = c.String(maxLength: 10),
                        Refunds = c.Double(),
                        AreaId = c.Int(),
                    })
                .PrimaryKey(t => t.BillOutId)
                .ForeignKey("dbo.Areas", t => t.AreaId)
                .ForeignKey("dbo.Customers", t => t.CustomerPhone)
                .ForeignKey("dbo.Employees", t => t.EmployeeId)
                .Index(t => t.EmployeeId)
                .Index(t => t.CustomerPhone)
                .Index(t => t.AreaId);
            
            CreateTable(
                "dbo.BillOutDetails",
                c => new
                    {
                        BillOutId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        QuantityProduct = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BillOutId, t.ProductId })
                .ForeignKey("dbo.BillOuts", t => t.BillOutId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.BillOutId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        ProductName = c.String(nullable: false, maxLength: 50),
                        ProductInfo = c.String(),
                        ProductImage = c.String(),
                        ProductPrice = c.Double(nullable: false),
                        TypeProductId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.TypeProducts", t => t.TypeProductId, cascadeDelete: true)
                .Index(t => t.TypeProductId);
            
            CreateTable(
                "dbo.Stocks",
                c => new
                    {
                        AreaId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        QuantityStock = c.Int(),
                    })
                .PrimaryKey(t => new { t.AreaId, t.ProductId })
                .ForeignKey("dbo.Areas", t => t.AreaId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.AreaId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.TypeProducts",
                c => new
                    {
                        TypeProductId = c.Int(nullable: false, identity: true),
                        TypeProductName = c.String(nullable: false, maxLength: 30),
                        TypeProductIcon = c.String(),
                    })
                .PrimaryKey(t => t.TypeProductId);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerPhone = c.String(nullable: false, maxLength: 15),
                        CustomerName = c.String(nullable: false, maxLength: 30),
                        RankId = c.Int(),
                    })
                .PrimaryKey(t => t.CustomerPhone)
                .ForeignKey("dbo.Ranks", t => t.RankId)
                .Index(t => t.RankId);
            
            CreateTable(
                "dbo.Ranks",
                c => new
                    {
                        RankId = c.Int(nullable: false, identity: true),
                        RankName = c.String(nullable: false, maxLength: 30),
                        NoteRank = c.String(maxLength: 30),
                        DiscountRank = c.Single(),
                    })
                .PrimaryKey(t => t.RankId);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        EmployeeId = c.Int(nullable: false, identity: true),
                        EmployeeName = c.String(nullable: false, maxLength: 30),
                        EmployeePhone = c.String(maxLength: 15),
                        EmployeeGender = c.Boolean(),
                        EmployeeAddress = c.String(),
                        EmployeeDate = c.DateTime(nullable: false),
                        EmployeeRole = c.Boolean(),
                        EmployeeEmail = c.String(),
                        EmployeeAvatar = c.String(),
                        PositionId = c.Int(),
                        UserName = c.String(maxLength: 20),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.EmployeeId)
                .ForeignKey("dbo.Positions", t => t.PositionId)
                .Index(t => t.PositionId);
            
            CreateTable(
                "dbo.Positions",
                c => new
                    {
                        PositionId = c.Int(nullable: false, identity: true),
                        PositionName = c.String(nullable: false, maxLength: 30),
                        QuantityEmployee = c.Int(nullable: false),
                        AreaId = c.Int(),
                    })
                .PrimaryKey(t => t.PositionId)
                .ForeignKey("dbo.Areas", t => t.AreaId)
                .Index(t => t.AreaId);
            
            CreateTable(
                "dbo.Tables",
                c => new
                    {
                        TableId = c.Int(nullable: false, identity: true),
                        TableName = c.String(nullable: false, maxLength: 50),
                        TableStatus = c.String(),
                        CustomerName = c.String(maxLength: 20),
                        ProductName = c.String(maxLength: 50),
                        AreaId = c.Int(),
                    })
                .PrimaryKey(t => t.TableId)
                .ForeignKey("dbo.Areas", t => t.AreaId)
                .Index(t => t.AreaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tables", "AreaId", "dbo.Areas");
            DropForeignKey("dbo.Employees", "PositionId", "dbo.Positions");
            DropForeignKey("dbo.Positions", "AreaId", "dbo.Areas");
            DropForeignKey("dbo.BillOuts", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.Customers", "RankId", "dbo.Ranks");
            DropForeignKey("dbo.BillOuts", "CustomerPhone", "dbo.Customers");
            DropForeignKey("dbo.Products", "TypeProductId", "dbo.TypeProducts");
            DropForeignKey("dbo.Stocks", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Stocks", "AreaId", "dbo.Areas");
            DropForeignKey("dbo.BillOutDetails", "ProductId", "dbo.Products");
            DropForeignKey("dbo.BillOutDetails", "BillOutId", "dbo.BillOuts");
            DropForeignKey("dbo.BillOuts", "AreaId", "dbo.Areas");
            DropIndex("dbo.Tables", new[] { "AreaId" });
            DropIndex("dbo.Positions", new[] { "AreaId" });
            DropIndex("dbo.Employees", new[] { "PositionId" });
            DropIndex("dbo.Customers", new[] { "RankId" });
            DropIndex("dbo.Stocks", new[] { "ProductId" });
            DropIndex("dbo.Stocks", new[] { "AreaId" });
            DropIndex("dbo.Products", new[] { "TypeProductId" });
            DropIndex("dbo.BillOutDetails", new[] { "ProductId" });
            DropIndex("dbo.BillOutDetails", new[] { "BillOutId" });
            DropIndex("dbo.BillOuts", new[] { "AreaId" });
            DropIndex("dbo.BillOuts", new[] { "CustomerPhone" });
            DropIndex("dbo.BillOuts", new[] { "EmployeeId" });
            DropTable("dbo.Tables");
            DropTable("dbo.Positions");
            DropTable("dbo.Employees");
            DropTable("dbo.Ranks");
            DropTable("dbo.Customers");
            DropTable("dbo.TypeProducts");
            DropTable("dbo.Stocks");
            DropTable("dbo.Products");
            DropTable("dbo.BillOutDetails");
            DropTable("dbo.BillOuts");
            DropTable("dbo.Areas");
        }
    }
}
