namespace coffeeapp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateBillOutDetail : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BillOutDetails", "ProductId", "dbo.Products");
            DropForeignKey("dbo.BillOutDetails", "BillOutId", "dbo.BillOuts");
            DropIndex("dbo.BillOutDetails", new[] { "ProductId" });
            DropPrimaryKey("dbo.BillOutDetails");
            AddColumn("dbo.BillOutDetails", "BillPriceTotal", c => c.Double());
            AddPrimaryKey("dbo.BillOutDetails", "BillOutId");
            AddForeignKey("dbo.BillOutDetails", "BillOutId", "dbo.BillOuts", "BillOutId");
            DropColumn("dbo.BillOutDetails", "ProductId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BillOutDetails", "ProductId", c => c.Int(nullable: false));
            DropForeignKey("dbo.BillOutDetails", "BillOutId", "dbo.BillOuts");
            DropPrimaryKey("dbo.BillOutDetails");
            DropColumn("dbo.BillOutDetails", "BillPriceTotal");
            AddPrimaryKey("dbo.BillOutDetails", new[] { "BillOutId", "ProductId" });
            CreateIndex("dbo.BillOutDetails", "ProductId");
            AddForeignKey("dbo.BillOutDetails", "BillOutId", "dbo.BillOuts", "BillOutId", cascadeDelete: true);
            AddForeignKey("dbo.BillOutDetails", "ProductId", "dbo.Products", "ProductId", cascadeDelete: true);
        }
    }
}
