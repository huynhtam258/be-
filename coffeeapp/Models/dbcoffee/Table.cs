﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace coffeeapp.Models.dbcoffee
{
    [Table("Tables")]
    public class Table
    {
        [Key]
        public int TableId { set; get; }

        [Required]
        [MaxLength(50)]
        public string TableName { set; get; }

        public string TableStatus { set; get; }

        [MaxLength(20)]
        public string CustomerName { set; get; }

        [MaxLength(50)]
        public string ProductName { set; get; }

        public int? AreaId { set; get; }
        [ForeignKey("AreaId")]
        public virtual Area Areas { set; get; }
    }
}