﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace coffeeapp.Models.dbcoffee
{
    [Table("Ranks")]
    public class Rank
    {
        [Key]
        public int RankId { set; get; }

        public virtual ICollection<Customer> Customers { set; get; }

        [Required]
        [MaxLength(30)]
        public string RankName { set; get; }

        [MaxLength(30)]
        public string NoteRank { set; get; }

        public float? DiscountRank { set; get; }
    }
}