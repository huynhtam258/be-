﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace coffeeapp.Models.dbcoffee
{
    [Table("BillOuts")]
    public class BillOut
    {
        [Key]
        public int BillOutId { set; get; }

        public virtual ICollection<BillOutDetail> BillOutDetails { set; get; }

        public int? EmployeeId { set; get; }

        [ForeignKey("EmployeeId")]
        public virtual Employee Employees { set; get; }

        [MaxLength(15)]
        public string CustomerPhone { set; get; }
        [ForeignKey("CustomerPhone")]
        public virtual Customer Customers { set; get; }

        public DateTime BillCreateDate { set; get; }

        public double? BillPriceTotal { set; get; }
        
        [MaxLength(10)]
        public string PaymentMethod { set; get; }

        public double? Refunds { set; get; }

        public int? AreaId { set; get; }

        [ForeignKey("AreaId")]
        public virtual Area Areas { set; get; }
        
    }
}