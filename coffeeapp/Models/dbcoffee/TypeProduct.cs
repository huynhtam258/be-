﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace coffeeapp.Models.dbcoffee
{
    [Table("TypeProducts")]
    public class TypeProduct
    {
        [Key]
        public int TypeProductId { set; get; }

        public virtual ICollection<Product> Products { set; get; }

        [Required]
        [MaxLength(30)]
        public string TypeProductName { set; get; }

        public string TypeProductIcon { set; get; }
    }
}