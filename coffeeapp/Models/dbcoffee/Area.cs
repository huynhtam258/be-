﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace coffeeapp.Models.dbcoffee
{
    [Table("Areas")]
    public class Area
    {
        [Key]
        public int AreaId { set; get; }
        public virtual ICollection<Table> Tables { set; get; }
        public virtual ICollection<Stock> Stocks { set; get; }
        public virtual ICollection<Position> Positions { set; get; }
        public virtual ICollection<BillOut> BillOuts { set; get; }

        [Required]
        [MaxLength(50)]
        public string AreaName { set; get; }

        [MaxLength(100)]
        public string AreaAdress { set; get; }

        [MaxLength(15)]
        public string AreaPhone { set; get; }
    }
}