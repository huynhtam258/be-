﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace coffeeapp.Models.dbcoffee
{
    [Table("Positions")]
    public class Position
    {
        [Key]
        public int PositionId { set; get; }

        public virtual ICollection<Employee> Employees { set; get; }

        [Required]
        [MaxLength(30)]
        public string PositionName { set; get; }

        public int QuantityEmployee { set; get; }

        public int? AreaId { set; get; }

        [ForeignKey("AreaId")]
        public virtual Area Areas { set; get; }
    }
}