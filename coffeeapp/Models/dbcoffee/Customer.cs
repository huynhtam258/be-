﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace coffeeapp.Models.dbcoffee
{
    [Table("Customers")]
    public class Customer
    {
        [Key]
        [MaxLength(15)]
        public string CustomerPhone { set; get; }

        public virtual ICollection<BillOut> BillOuts { set; get; }

        [Required]
        [MaxLength(30)]
        public string CustomerName { set; get; }

        public int? RankId { set; get; }

        [ForeignKey("RankId")]
        public virtual Rank Ranks { set; get; }
    }
}