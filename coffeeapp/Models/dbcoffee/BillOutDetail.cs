﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace coffeeapp.Models.dbcoffee
{
    [Table("BillOutDetails")]
    public class BillOutDetail
    {
        [Key/*, Column(Order = 1)*/]
        public int BillOutId { set; get; }

        //[Key, Column(Order = 2)]
        //public int ProductId { set; get; }

        public int QuantityProduct { set; get; }

        public double? BillPriceTotal { set; get; }

        [ForeignKey("BillOutId")]
        public virtual BillOut BillOuts { set; get; }

        //[ForeignKey("ProductId")]
        //public virtual Product Products { set; get; }
    }
}