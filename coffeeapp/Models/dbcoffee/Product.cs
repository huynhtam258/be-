﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace coffeeapp.Models.dbcoffee
{
    [Table("Products")]
    public class Product
    {
        [Key]
        public int ProductId { set; get; }

        //public virtual ICollection<BillOutDetail> BillOutDetails { set; get; }
        public virtual ICollection<Stock> Stocks { set; get; }

        [Required]
        [MaxLength(50)]
        public string ProductName { set; get; }

        public string ProductInfo { set; get; }

        public string ProductImage { set; get; }

        public double ProductPrice { set; get; }
        
        public int TypeProductId { set; get; }

        [ForeignKey("TypeProductId")]
        public virtual TypeProduct TypeProducts { set; get; }
    }
}