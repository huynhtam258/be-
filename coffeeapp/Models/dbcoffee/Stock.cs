﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace coffeeapp.Models.dbcoffee
{
    [Table("Stocks")]
    public class Stock
    {
        [Key, Column(Order = 1)]
        public int AreaId { set; get; }

        [ForeignKey("AreaId")]
        public virtual Area Areas { set; get; }

        [Key, Column(Order = 2)]
        public int ProductId { set; get; }

        [ForeignKey("ProductId")]
        public virtual Product Products { set; get; }

        public int? QuantityStock { set; get; }
    }
}