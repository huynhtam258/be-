﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace coffeeapp.Models.dbcoffee
{
    [Table("Employees")]
    public class Employee
    {
        [Key]
        public int EmployeeId { set; get; }

        public virtual ICollection<BillOut> BillOuts { set; get; }

        [Required]
        [MaxLength(30)]
        public string EmployeeName { set; get; }

        [MaxLength(15)]
        public string EmployeePhone { set; get; }

        public bool? EmployeeGender { set; get; }

        public string EmployeeAddress { set; get; }

        public DateTime EmployeeDate { set; get; }

        public bool? EmployeeRole { set; get; }

        public string EmployeeEmail { set; get; }

        public string EmployeeAvatar { set; get; }

        public int? PositionId { set; get; }

        [ForeignKey("PositionId")]
        public virtual Position Positions { set; get; }

        [MaxLength(20)]
        public string UserName { set; get; }
        public string Password { set; get; }
    }
}