﻿using coffeeapp.DTO;
using coffeeapp.Models.dbcoffee;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;

namespace coffeeapp.Controllers
{
    [RoutePrefix("api/Employees")]
    public class EmployeesController : ApiController
    {
        private CoffeeDbContext db = new CoffeeDbContext();

        //[HttpGet]
        //[Route("GetEmployees")]
        //public IQueryable<Employee> GetEmployees()
        //{
        //    return db.Employees;
        //}

        /**
         * @api {get} Employees/GetEmployee xuat ra thong tin nhan vien.
         * @apiVersion 0.1.0
         * @apiName GetEmployee
         * @apiGroup Employees
         * 
         * @apiParam {string} UserName Ten dang nhap cua tai khoan
         * 
         * @apiSuccess {string} UserName ten dang nhap
         * @apiSuccess {string} Avartar hinh dai dien
         * @apiSuccess {string} PhoneNumber so dien thoai cua nhan vien
         * @apiSuccess {string} Email email cua nhan vien
         * 
         * @apiSuccessExample {json} Success-Response:
         *      HTTP/1.1 200 OK
         *     {
         *       "UserName":"admin",
         *       "Avartar":null,
         *       "PhoneNumber":"0123465"
         *       "Email": "admin@gmail.com"
         *       "Password":"FnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Lu"
         *     }
         * @apiError (400) BadRequest loi Request truyen vao
         * 
         * @apiErrorExample Error-Request:
         *     HTTP/1.1 400 Bad Request
         *     {
         *       "Message": "The request is invalid."
         *     }
         *
         * @apiError (404) NotFound UserName truyen vao khong ton tai
         * 
         */
        [AllowAnonymous]
        [Authorize]
        [HttpGet]
        [Route("GetEmployee")]
        [ResponseType(typeof(Employee))]
        public IHttpActionResult GetEmployee(string UserName)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var employeeDto = from emp in db.Employees
                              where emp.UserName == UserName
                              select new EmployeeDTO
                              {
                                  UserName = emp.UserName,
                                  Avartar = emp.EmployeeAvatar,
                                  PhoneNumber = emp.EmployeePhone,
                                  Email = emp.EmployeeEmail,
                                  Password = emp.Password
                              };
            if (employeeDto == null)
            {
                return NotFound();
            }
            return Ok(employeeDto);
        }

        //[HttpPost]
        //[Route("PostEmployee")]
        //[ResponseType(typeof(Employee))]
        //public IHttpActionResult PostEmployee(Employee employee)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Employees.Add(employee);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = employee.EmployeeId }, employee);
        //}

        //[HttpDelete]
        //[Route("DeleteEmployee")]
        //[ResponseType(typeof(Employee))]
        //public IHttpActionResult DeleteEmployee(int id)
        //{
        //    Employee employee = db.Employees.Find(id);
        //    if (employee == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Employees.Remove(employee);
        //    db.SaveChanges();

        //    return Ok(employee);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}



        private const string Secret = "db3OIsj+BXE9NZDy0t8W3TcNekrF+2d/1sFnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Luqm/hccMw==";

        public static string createToken(string username, int expireMinutes = 20)
        {
            var symmetricKey = Convert.FromBase64String(Secret);
            var tokenHandler = new JwtSecurityTokenHandler();

            var now = DateTime.UtcNow;
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                        {
                        new Claim(ClaimTypes.Name, username)
                    }),

                Expires = now.AddMinutes(Convert.ToInt32(expireMinutes)),

                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256Signature)
            };

            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);

            return token;
        }

        public string HashPass(string password)
        {

            byte[] encodedPassword = new UTF8Encoding().GetBytes(password);
            byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);
            string encoded = BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();

            return encoded;//returns hashed version of password
        }

        /**
         * @api {put} /Employees/Register dang ki nhan vien
         * @apiVersion 0.1.0
         * @apiName Register
         * @apiGroup Employees
         * 
         * @apiHeader {string} EmployeeName ten nhan vien
         * @apiHeader {string} EmployeePhone sdt nhan vien
         * @apiHeader {string} EmployeeEmail mail nv
         * @apiHeader {string} Username mat khau
         * @apiHeader {string} Password mat khau
         * 
         * @apiHeaderExample {json} Header-Example:
         *      {
         *          "EmployeeName":"admin",
         *          "EmployeePhone":"123456",,
         *          "EmployeeEmail":"admin@mail"
         *          "Password":"admin123"
         *          "UserName":"admin"
         *      }
         * 
         * @apiError (400) BadRequest loi request truyen vao
         * 
         * @apiErrorExample Error-Request:
         *     HTTP/1.1 400 Bad Request
         *     {
         *       "Message": "The request is invalid."
         *     }
         *     
         */
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("Register")]
        [ResponseType(typeof(Employee))]
        public IHttpActionResult Register(Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            var emp = new Employee
            {
                EmployeeName = employee.EmployeeName,
                EmployeePhone = employee.EmployeePhone,
                //EmployeeGender = employee.EmployeeGender,
                //EmployeeAddress = employee.EmployeeAddress,
                //EmployeeDate = employee.EmployeeDate,
                //EmployeeRole = employee.EmployeeRole,
                EmployeeEmail = employee.EmployeeEmail,
                //EmployeeAvatar = employee.EmployeeAvatar,
                //PositionId = employee.PositionId,
                UserName = employee.UserName,
                Password = HashPass(employee.Password)
        };
            db.Employees.Add(emp);
            db.SaveChanges();

            return Ok("Dang ky thanh cong");
        }

        /**
         * @api {put} /Employees/PutEmployee Sua du lieu nhan vien
         * @apiVersion 0.1.0
         * @apiName PutEmployee
         * @apiGroup Employees
         * 
         * @apiParam {string} UserName tên dăng nhâp
         * 
         * @apiHeader {string} EmployeeName ten nhan vien
         * @apiHeader {string} EmployeePhone sdt nhan vien
         * @apiHeader {string} EmployeeEmail mail nv
         * @apiHeader {string} Password mat khau
         * 
         * @apiHeaderExample {json} Header-Example:
         *      {
         *          "EmployeeName":"admin",
         *          "EmployeePhone":"123456",,
         *          "EmployeeEmail":"admin@mail"
         *          "Password":"admin123"
         *      }
         * 
         * @apiError (400) BadRequest loi request truyen vao
         * 
         * @apiErrorExample Error-Request:
         *     HTTP/1.1 400 Bad Request
         *     {
         *       "Message": "The request is invalid."
         *     }
         *     
         * @apiError (404) NotFound id truyen vao khong ton tai
         */
        [AllowAnonymous]
        [Authorize]
        [HttpPut]
        [Route("PutEmployee")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEmployee(string UserName, Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!EmployeeExists1(UserName))
            {
                return NotFound();
            }

            var emp = db.Employees.FirstOrDefault(a => a.UserName == UserName);
            emp.EmployeeName = employee.EmployeeName;
            emp.EmployeePhone = employee.EmployeePhone;
            //emp.EmployeeGender = employee.EmployeeGender;
            //emp.EmployeeAddress = employee.EmployeeAddress;
            //emp.EmployeeDate = employee.EmployeeDate;
            //emp.EmployeeRole = employee.EmployeeRole;
            emp.EmployeeEmail = employee.EmployeeEmail;
            //emp.EmployeeAvatar = employee.EmployeeAvatar;
            //emp.PositionId = employee.PositionId;
            //emp.UserName = employee.UserName;
            emp.Password = HashPass(employee.Password);
            db.SaveChanges();
            return StatusCode(HttpStatusCode.NoContent);
        }

        /**
         * @api {post} /employees/login Dang nhap voi userName va password    
         * @apiVersion 0.1.0
         * @apiName login
         * @apiGroup employees
         * 
         * @apiParam {String} userName ten dang nhap
         * @apiParam {String} password mat khau dang nhap
         * 
         * @apiSuccess {String} token xuat ra chuoi token
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *     {
         *       "e30.eyJuYmYiOjE1NDM5ODIwNTIsImV4cCI6MTU0Mzk4NTY1MiwiaWF0IjoxNTQzOTgyMDUyLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjU3ODMzIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDo1NzgzMyJ9."
         *     }
         * 
         * @apiError (400) BadRequest khi khong co du lieu nhap vao
         * 
         * @apiErrorExample Error-Request:
         *     HTTP/1.1 400 Bad Request
         *     {
         *          "Message": "The request is invalid.",
         *          "ModelState": {
         *               "userName.String": [
         *                   "A value is required but was not present in the request."
         *              ],
         *              "password.String": [
         *                  "A value is required but was not present in the request."
         *              ]
         *           }
         *       }
         *     
         * @apiError (400) BadRequest khi sai tai khoan mat khau
         * 
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 400 Bad Request
         *     {
         *       "Message": "not found"
         *     }
         */
        [AllowAnonymous]
        [Authorize]
        [Route("Login")]
        [ResponseType(typeof(Employee))]
        [HttpPost]
        public IHttpActionResult Login(string userName, string Password)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string readHash = HashPass(Password);
            Employee employee = db.Employees.SingleOrDefault(a => a.UserName.Equals(userName) && a.Password.Equals(readHash));
            if (employee == null)
            {
                return NotFound();
            }
            //return StatusCode(HttpStatusCode.OK);
            string token = createToken(userName);
            return Ok<string>(token);

        }

        private bool EmployeeExists(int id)
        {
            return db.Employees.Count(e => e.EmployeeId == id) > 0;
        }
        private bool EmployeeExists1(string UserName)
        {
            return db.Employees.Count(e => e.UserName == UserName) > 0;
        }
    }
}