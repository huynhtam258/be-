﻿using coffeeapp.DTO;
using coffeeapp.Models.dbcoffee;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace coffeeapp.Controllers
{
    [RoutePrefix("api/BillOutDetails")]
    public class BillOutDetailsController : ApiController
    {
        private CoffeeDbContext db = new CoffeeDbContext();

        //[HttpGet]
        //[Route("GetBillOutDetails")]
        //public IQueryable<BillOutDetail> GetBillOutDetails()
        //{
        //    return db.BillOutDetails;
        //}

        /**
         * @api {get} /BillOutDetails/GetBillOutDetail xuat chi tiet hoa don theo id hoa don
         * @apiVersion 0.1.0
         * @apiName GetBillOutDetail
         * @apiGroup BillOutDetails
         * 
         * @apiParam {int} id id cua hoa don
         * 
         * @apiSuccess {int} BillOutId Ma cua hoa don
         * @apiSuccess {string} ProductName ten san pham
         * @apiSuccess {int} QuantityProduct So luong san pham
         * @apiSuccess {double} Money tong tien cua san pham
         * 
         * @apiSuccessExample {json} Success-Response:
         *      HTTP/1.1 200 OK
         *     {
         *       "BillOutId":1,
         *       "QuantityProduct":3
         *       "BillPriceTotal": 30000
         *     }
         *     
         * @apiError (400) BadRequest loi Request truyen vao
         * 
         * @apiErrorExample Error-Request:
         *     HTTP/1.1 400 Bad Request
         *     {
         *       "Message": "The request is invalid."
         *     }
         *
         * @apiError (404) NotFound id truyen vao khong ton tai
         * 
         */
        [AllowAnonymous]
        [Authorize]
        [HttpGet]
        [Route("GetBillOutDetail")]
        [ResponseType(typeof(BillOutDetail))]
        public IHttpActionResult GetBillOutDetail(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var billOutDetailDto = from bd in db.BillOutDetails
                                   where bd.BillOutId == id
                                   select new BillOutDetailDTO
                                   {
                                       BillOutId = bd.BillOutId,
                                       QuantityProduct = bd.QuantityProduct,
                                       BillPriceTotal = bd.BillPriceTotal
                                   };
            if (billOutDetailDto == null)
            {
                return NotFound();
            }

            return Ok(billOutDetailDto);
        }

        /**
         * @api {put} /BillOutDetails/PutBillOutDetail Sua du lieu trong chi tiet hoa don
         * @apiVersion 0.1.0
         * @apiName PutBillOutDetail
         * @apiGroup BillOutDetails
         * 
         * @apiParam {int} billoutId id cua hoa don can sua
         * 
         * @apiHeader {int} QuantityProduct Nhap vao so luong  san pham
         * @apiHeader {int} BillPriceTotal Nhap vao tong tien
         * 
         * @apiHeaderExample {json} Header-Example:
         *      {
         *          "QuantityProduct":3,
         *          "BillPriceTotal":200000,
         *      }
         * 
         * @apiError (400) BadRequest loi request truyen vao
         * 
         * @apiErrorExample Error-Request:
         *     HTTP/1.1 400 Bad Request
         *     {
         *       "Message": "The request is invalid."
         *     }
         *     
         * @apiError (404) NotFound id truyen vao khong ton tai
         */
        [AllowAnonymous]
        [Authorize]
        [HttpPut]
        [Route("PutBillOutDetail")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutBillOutDetail(int billoutId,[FromBody] BillOutDetail billOutDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!BillOutDetailExists(billoutId))
            {
                return NotFound();
            }

            var bod = db.BillOutDetails.FirstOrDefault(a => a.BillOutId == billoutId);
            bod.QuantityProduct = billOutDetail.QuantityProduct;
            bod.BillPriceTotal = billOutDetail.BillPriceTotal;
            db.SaveChanges();
            return StatusCode(HttpStatusCode.NoContent);
        }

        /**
         * @api {post} /BillOutDetails/PostBillOutDetail Them du lieu trong chi tiet hoa don
         * @apiVersion 0.1.0
         * @apiName PostBillOutDetail
         * @apiGroup BillOutDetails
         * 
         * @apiHeader {int} BillOutId Nhap vao ma cua hoa don*
         * @apiHeader {int} ProductId Nhap vao ma cua san pham
         * @apiHeader {int} QuantityProduct Nhap vao so luong  san pham 
         * 
         * @apiHeaderExample {json} Header-Example:
         *      {
         *          "QuantityProduct":3
         *          "BillPriceTotal":20000
         *      }
         *      
         * @apiError (400) BadRequest loi request truyen vao
         * 
         * @apiErrorExample Error-Request:
         *     HTTP/1.1 400 Bad Request
         *     {
         *       "Message": "The request is invalid."
         *     }
         *     
         * @apiError (409) Conflict
         * 
         * @apiSuccess {int} BillOutId Ma so hoa don
         * @apiSuccess {int} ProductId Ma so san pham
         * @apiSuccess {int} QuantityProduct So luong san pham
         * 
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *     {
         *       "BillOutId": 1,
         *       "QuantityProduct": 3,
         *       "BillPriceTotal":20000
         *     }
         * 
         */
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("PostBillOutDetail")]
        [ResponseType(typeof(BillOutDetail))]
        public IHttpActionResult PostBillOutDetail(BillOutDetail billOutDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                if(BillOutDetailExists(billOutDetail.BillOutId))
                {
                    return Conflict();
                }
                else
                {
                    db.BillOutDetails.Add(billOutDetail);
                    db.SaveChanges();
                    return Ok(billOutDetail);
                }
                
            }
            
        }

        //[HttpDelete]
        //[Route("DeleteBillOutDetail")]
        //[ResponseType(typeof(BillOutDetail))]
        //public IHttpActionResult DeleteBillOutDetail(int id)
        //{
        //    BillOutDetail billOutDetail = db.BillOutDetails.Find(id);
        //    if (billOutDetail == null)
        //    {
        //        return NotFound();
        //    }

        //    db.BillOutDetails.Remove(billOutDetail);
        //    db.SaveChanges();

        //    return Ok(billOutDetail);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BillOutDetailExists(int billoutid)
        {
            return (db.BillOutDetails.Count(e => e.BillOutId == billoutid) > 0 );
        }
    }
}