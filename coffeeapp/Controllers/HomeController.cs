﻿using System.Web.Mvc;

namespace coffeeapp.Controllers
{
    public class HomeController : Controller
    {
        private CoffeeDbContext db = new CoffeeDbContext();

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        //public ActionResult Login(FormCollection f)
        //{
        //    string sUserName = f["uname"].ToString();
        //    string sPassword = f["psw"].ToString();
        //    Employee nv = db.Employees.SingleOrDefault(n => n.UserName == sUserName && n.Password == sPassword);
        //    if (nv != null)
        //    {
        //        Session["TaiKhoan"] = nv;
        //        return RedirectToAction("Index");
        //    }

        //    return RedirectToAction("Index");
        //}
    }
}