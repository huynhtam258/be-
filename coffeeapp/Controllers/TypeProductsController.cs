﻿using coffeeapp.DTO;
using coffeeapp.Models.dbcoffee;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace coffeeapp.Controllers
{
    [RoutePrefix("api/TypeProducts")]
    public class TypeProductsController : ApiController
    {
        public CoffeeDbContext db = new CoffeeDbContext();
        /**
        * @api {get} /TypeProducts/GetTypeProducts Tra loai san pham
        * @apiVersion 0.1.0
        * @apiName GetTypeProducts
        * @apiGroup TypeProducts
        * 
        * @apiSuccess {int} typeProduct loai san pham
        * @apiSuccessExample {json} Success-Response:
        *      HTTP/1.1 200 OK
        *     {
        *          "TypeProductId": 1,
        *          "TypeProductName": "coffee",
        *          "TypeProductIcon": "cf"
        *     }
        *     
        * @apiError (404) NotFound id truyen vao khong ton tai
        * @apiErrorExample Error-Request:
         *     HTTP/1.1 404 Not Found
         *     {
         *       "Message": "Not Value"
         *     }
        */
        [AllowAnonymous]
        [Authorize]
        [HttpGet]
        [Route("GetTypeProducts")]
        public HttpResponseMessage GetTypeProducts()
        {
            var typeproductDto = from pd in db.TypeProducts
                                 select new TypeProductDTO
                                 {
                                     TypeProductId = pd.TypeProductId,
                                     TypeProductName = pd.TypeProductName,
                                     TypeProductIcon = pd.TypeProductIcon
                                 };
            if (typeproductDto.Count()>0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, typeproductDto);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Not Value");
            }
        }


        //[HttpGet]
        //[Route("GetTypeProduct")]
        //[ResponseType(typeof(TypeProduct))]
        //public IHttpActionResult GetTypeProduct(int id)
        //{
        //    TypeProduct typeproduct = db.TypeProducts.Find(id);
        //    if (typeproduct == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(typeproduct);
        //}

        //[HttpPut]
        //[Route("PutTypeProduct")]
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutTypeProduct(int id, TypeProduct typeProduct)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != typeProduct.TypeProductId)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(typeProduct).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!TypeProductExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //[HttpPost]
        //[Route("PostTypeProduct")]
        //[ResponseType(typeof(TypeProduct))]
        //public IHttpActionResult PostTypeProduct(TypeProduct typeProduct)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.TypeProducts.Add(typeProduct);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = typeProduct.TypeProductId }, typeProduct);
        //}

        //[HttpDelete]
        //[Route("DeleteTypeProduct")]
        //[ResponseType(typeof(TypeProduct))]
        //public IHttpActionResult DeleteTypeProduct(int id)
        //{
        //    TypeProduct typeProduct = db.TypeProducts.Find(id);
        //    if (typeProduct == null)
        //    {
        //        return NotFound();
        //    }

        //    db.TypeProducts.Remove(typeProduct);
        //    db.SaveChanges();

        //    return Ok(typeProduct);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool TypeProductExists(int id)
        //{
        //    return db.TypeProducts.Count(e => e.TypeProductId == id) > 0;
        //}

        //[Route("GetImage")]
        //[ResponseType(typeof(TypeProduct))]
        //public IHttpActionResult GetImage(int id)
        //{
        //    TypeProduct typeProduct = db.TypeProducts.Find(id);
        //    if (typeProduct == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(typeProduct.TypeProductIcon);
        //}
    }
}