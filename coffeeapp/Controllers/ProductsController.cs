﻿using coffeeapp.DTO;
using coffeeapp.Models.dbcoffee;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace coffeeapp.Controllers
{
    [RoutePrefix("api/Products")]
    public class ProductsController : ApiController
    {
        public CoffeeDbContext db = new CoffeeDbContext();

        /**
         * @api {get} /Products/GetProductsByIdTypeProduct Tra san pham theo ma loai san pham
         * @apiVersion 0.1.0
         * @apiName GetProductsByIdTypeProduct
         * @apiGroup Products
         * 
         * @apiParam {int} id ma loai san pham
         * 
         * @apiSuccess {int} products Cac loai san pham
         * @apiSuccessExample {json} Success-Response:
         *      HTTP/1.1 200 OK
         *     {
         *          "ProductId": 6,
         *          "ProductName": "coffee da",
         *          "ProductInfo": "coffee+da",
         *          "ProductImage": "cfd",
         *          "ProductPrice": 10000,
         *          "TypeProductId": 1
         *     }
         * 
         * @apiError (400) BadRequest loi Request
         * 
         * @apiErrorExample Error-Request:
         *     HTTP/1.1 400 Bad Request
         *     {
         *       "Message": "The request is invalid."
         *     }
         *     
         * @apiError (404) NotFound id truyen vao khong ton
         * 
         * @apiErrorExample Error-Request:
         *     HTTP/1.1 404 Not Found
         *     {
         *       "Message": "Not found Product's ID"
         *     }
         */
        [AllowAnonymous]
        [Authorize]
        [HttpGet]
        [Route("GetProductsByIdTypeProduct")]
        [ResponseType(typeof(ProductDTO))]
        public HttpResponseMessage GetProductsByIdTypeProduct(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            var productDto = from pd in db.Products
                             where pd.TypeProductId == id
                             select new ProductDTO
                             {
                                 ProductId = pd.ProductId,
                                 ProductName = pd.ProductName,
                                 ProductInfo = pd.ProductInfo,
                                 ProductImage = pd.ProductImage,
                                 ProductPrice = pd.ProductPrice,
                                 TypeProductId = pd.TypeProductId
                             };
            if (productDto != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, productDto);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Not found Product's ID");
            }
        }

        /**
        * @api {get} /Products/GetTypeProducts Tra san pham theo ma san pham
        * @apiVersion 0.1.0
        * @apiName GetTypeProducts
        * @apiGroup Products
        * 
        * @apiParam {int} id ma san pham
        * 
        * @apiSuccess {int} product.ProductInfo thong tin san pham
        * @apiSuccessExample {json} Success-Response:
        *      HTTP/1.1 200 OK
        *     {
        *           "ProductId": 6,
         *          "ProductName": "coffee da",
         *          "ProductInfo": "coffee+da",
         *          "ProductImage": "cfd",
         *          "ProductPrice": 10000,
         *          "TypeProductId": 1
        *     }
        * 
        * @apiError (400) BadRequest Loi request
        * 
        * @apiErrorExample Error-Request:
        *     HTTP/1.1 400 Bad Request
        *     {
        *       "Message": "The request is invalid."
        *     }
        *     
        * @apiError (404) NotFound id truyen vao khong ton tai
        * 
        */
        [AllowAnonymous]
        [Authorize]
        [HttpGet]
        [Route("GetTypeProduct")]
        [ResponseType(typeof(Product))]
        public IHttpActionResult GetTypeProduct(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var productDto = from pd in db.Products
                             where pd.ProductId == id
                             select new ProductDTO
                             {
                                 ProductId = pd.ProductId,
                                 ProductName = pd.ProductName,
                                 ProductInfo = pd.ProductInfo,
                                 ProductImage = pd.ProductImage,
                                 ProductPrice = pd.ProductPrice,
                                 TypeProductId = pd.TypeProductId
                             };
            if (productDto == null)
            {
                return NotFound();
            }
            return Ok(productDto);
        }

        //[HttpGet]
        //[Route("GetProducts")]
        //public IQueryable<Product> GetProducts()
        //{
        //    return db.Products;
        //}

        //[HttpGet]
        //[Route("GetProducts1")]
        //public IQueryable<Product> GetProducts1(int id)
        //{
        //    return db.Products.Where(a => a.TypeProductId == id);
        //}

        //[HttpGet]
        //[Route("GetProductInfo")]
        //[ResponseType(typeof(Product))]
        //public IHttpActionResult GetProductInfo(int id)
        //{
        //    Product product = db.Products.Find(id);
        //    if (product == null)
        //    {
        //        return NotFound();
        //    }
        //    return Ok(product.ProductInfo);
        //}

        //[HttpGet]
        //[Route("GetProduct")]
        //[ResponseType(typeof(Product))]
        //public IHttpActionResult GetProduct(int id)
        //{
        //    Product product = db.Products.Find(id);
        //    if (product == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(product);
        //}

        //[HttpPut]
        //[Route("PutProduct")]
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutProduct(int id, Product product)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != product.ProductId)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(product).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!ProductExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //[HttpPost]
        //[Route("PostProduct")]
        //[ResponseType(typeof(Product))]
        //public IHttpActionResult PostProduct(Product product)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Products.Add(product);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = product.ProductId }, product);
        //}

        //[HttpDelete]
        //[Route("DeleteProduct")]
        //[ResponseType(typeof(Product))]
        //public IHttpActionResult DeleteProduct(int id)
        //{
        //    Product product = db.Products.Find(id);
        //    if (product == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Products.Remove(product);
        //    db.SaveChanges();

        //    return Ok(product);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool ProductExists(int id)
        //{
        //    return db.Products.Count(e => e.ProductId == id) > 0;
        //}
    }
}