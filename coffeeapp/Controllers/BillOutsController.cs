﻿using coffeeapp.Models.dbcoffee;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace coffeeapp.Controllers
{
    [RoutePrefix("api/BillOuts")]
    public class BillOutsController : ApiController
    {
        private CoffeeDbContext db = new CoffeeDbContext();

        /**
         * @api {get} /BillOuts/GetTotalMoney Tra ra tong so tien cua chi nhanh theo id
         * @apiVersion 0.1.0
         * @apiName GetTotalMoney
         * @apiGroup BillOuts
         * 
         * @apiParam {int} id id cua chi nhanh can tinh doang thu
         * 
         * @apiSuccess {int} money.Sum Tong doanh thu chi nhanh
         * @apiSuccessExample {json} Success-Response:
         *      HTTP/1.1 200 OK
         *     {
         *       450000
         *     }
         * 
         * @apiError (400) BadRequest khong co id truyen vao
         * 
         * @apiErrorExample Error-Request:
         *     HTTP/1.1 400 Bad Request
         *     {
         *       "Message": "The request is invalid."
         *     }
         *     
         * @apiError (404) NotFound id truyen vao khong ton tai
         * 
         */
        [AllowAnonymous]
        [Authorize]
        [HttpGet]
        [Route("GetTotalMoney")]
        [ResponseType(typeof(BillOut))]
        public IHttpActionResult GetTotalMoney(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (db.Areas.Count(a => a.AreaId == id) > 0)
            {
                var money = from mn in db.BillOuts
                            where mn.AreaId == id
                            select mn.BillPriceTotal;
                return Ok(money.Sum());
            }
            else
            {
                return NotFound();
            }
        }

        /**
         * @api {get} /BillOuts/GetTotalMoneyADay Tra ra tong so tien trong ngay cua chi nhanh theo id
         * @apiVersion 0.1.0
         * @apiName GetTotalMoneyADay
         * @apiGroup BillOuts
         * 
         * @apiParam {int} id id cua chi nhanh can tinh doang thu
         * @apiParam {int} day nhap ngay
         * @apiParam {int} mon nhap thang
         * @apiParam {int} year nhap nam
         * 
         * @apiSuccess {int} money.Sum Tong doanh thu chi nhanh
         * @apiSuccessExample {json} Success-Response:
         *      HTTP/1.1 200 OK
         *     {
         *       450000
         *     }
         * 
         * @apiError (400) BadRequest khong co id truyen vao
         * 
         * @apiErrorExample Error-Request:
         *     HTTP/1.1 400 Bad Request
         *     {
         *       "Message": "The request is invalid."
         *     }
         *     
         * @apiError (404) NotFound id truyen vao khong ton tai
         * 
         */
        [AllowAnonymous]
        [Authorize]
        [HttpGet]
        [Route("GetTotalMoneyADay")]
        [ResponseType(typeof(BillOut))]
        public IHttpActionResult GetTotalMoneyADay(int id, int day, int mon, int year)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (db.Areas.Count(a => a.AreaId == id) > 0)
            {
                var money = from mn in db.BillOuts
                            where mn.AreaId == id && mn.BillCreateDate.Day == day && mn.BillCreateDate.Month == mon && mn.BillCreateDate.Year == year
                            select mn.BillPriceTotal;
                return Ok(money.Sum());
            }
            else
            {
                return NotFound();
            }
        }

        /**
         * @api {get} /BillOuts/GetTotalMoneyAMonth Tra ra tong so tien trong thang cua chi nhanh theo id
         * @apiVersion 0.1.0
         * @apiName GetTotalMoneyAMonth
         * @apiGroup BillOuts
         * 
         * @apiParam {int} id id cua chi nhanh can tinh doang thu
         * @apiParam {int} mon nhap thang
         * @apiParam {int} year nhap nam
         * 
         * @apiSuccess {int} money.Sum Tong doanh thu chi nhanh
         * @apiSuccessExample {json} Success-Response:
         *      HTTP/1.1 200 OK
         *     {
         *       450000
         *     }
         * 
         * @apiError (400) BadRequest khong co id truyen vao
         * @apiError (404) NotFound id truyen vao khong ton tai
         * 
         * @apiErrorExample Error-Request:
         *     HTTP/1.1 400 Bad Request
         *     {
         *       "Message": "The request is invalid."
         *     }
         *     
         * @apiError (404) NotFound id truyen vao khong ton tai
         * 
         */
        [AllowAnonymous]
        [Authorize]
        [HttpGet]
        [Route("GetTotalMoneyAMonth")]
        [ResponseType(typeof(BillOut))]
        public IHttpActionResult GetTotalMoneyAMonth(int id, int mon, int year)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (db.Areas.Count(a => a.AreaId == id) > 0)
            {
                var money = from mn in db.BillOuts
                            where mn.AreaId == id && mn.BillCreateDate.Month == mon && mn.BillCreateDate.Year == year
                            select mn.BillPriceTotal;
                return Ok(money.Sum());
            }
            else
            {
                return NotFound();
            }
        }

        /**
         * @api {get} /BillOuts/GetTotalMoneyAYear Tra ra tong so tien trong nam cua chi nhanh theo id
         * @apiVersion 0.1.0
         * @apiName GetTotalMoneyAYear
         * @apiGroup BillOuts
         * 
         * @apiParam {int} id id cua chi nhanh can tinh doang thu
         * @apiParam {int} year nhap nam
         * 
         * @apiSuccess {int} money.Sum Tong doanh thu chi nhanh
         * @apiSuccessExample {json} Success-Response:
         *      HTTP/1.1 200 OK
         *     {
         *       450000
         *     }
         * 
         * @apiError (400) BadRequest khong co id truyen vao
         * @apiError (404) NotFound id truyen vao khong ton tai
         * 
         * @apiErrorExample Error-Request:
         *     HTTP/1.1 400 Bad Request
         *     {
         *       "Message": "The request is invalid."
         *     }
         *     
         * @apiError (404) NotFound id truyen vao khong ton tai
         * 
         */
        [AllowAnonymous]
        [Authorize]
        [HttpGet]
        [Route("GetTotalMoneyAYear")]
        [ResponseType(typeof(BillOut))]
        public IHttpActionResult GetTotalMoneyAYear(int id, int year)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (db.Areas.Count(a => a.AreaId == id) > 0)
            {
                var money = from mn in db.BillOuts
                            where mn.AreaId == id && mn.BillCreateDate.Year == year
                            select mn.BillPriceTotal;
                return Ok(money.Sum());
            }
            else
            {
                return NotFound();
            }
        }

        /**
         * @api {put} /BillOuts/PutBillOut Sua du lieu trong hoa don
         * @apiVersion 0.1.0
         * @apiName PutBillOut
         * @apiGroup BillOuts
         * 
         * @apiParam {int} id id cua hoa don can sua
         *
         * @apiHeader {int} EmployeeId Nhap vao ma cua Nhan vien
         * @apiHeader {string} CustomerPhone Nhap vao sdt cua khach hang
         * @apiHeader {DateTime} BillCreateDate Nhap vao ngay tao hoa don
         * @apiHeader {double} BillPriceTotal Nhap vao tong so tien cua hoa don
         * @apiHeader {string} PaymentMethod Nhap vao phuong thuc tra tien
         * @apiHeader {double} Refunds Nhap vao so tien tra lai
         * @apiHeader {int} AreaId Nhap vao ma chi nhanh
         * 
         * @apiHeaderExample {json} Header-Example:
         *      {
         *          "EmployeeId": 1,
         *           "CustomerPhone": null,
         *           "BillCreateDate": "2018/12/14",
         *           "BillPriceTotal": 20000,
         *           "PaymentMethod": null,
         *           "Refunds": null,
         *           "AreaId": null,
         *      }
         *  
         * @apiSuccess {int} BillOutId Ma cua hoa don
         * @apiSuccess {int} EmployeeId ma cua Nhan vien
         * @apiSuccess {string} CustomerPhone sdt cua khach hang
         * @apiSuccess {DateTime} BillCreateDate ngay tao hoa don
         * @apiSuccess {double} BillPriceTotal tong so tien cua hoa don
         * @apiSuccess {string} PaymentMethod phuong thuc tra tien
         * @apiSuccess {double} Refunds so tien tra lai
         * @apiSuccess {int} AreaId ma chi nhanh
         * 
         * @apiSuccessExample {json} Success-Response:
         *      HTTP/1.1 200 OK
         *     {
         *          "BillOutId":26,
         *          "EmployeeId":1,
         *          "CustomerPhone":"01234",
         *          "BillCreateDate": "2011/11/11",
         *          "BillPriceTotal":20000,
         *          "PaymentMethod":"tien mat",
         *          "Refunds":0,
         *          "AreaId":1
         *          "Customers": null,
         *          "Employees": null
         *          "Areas": null
         *     }
         * 
         * @apiError (400) BadRequest khong co id truyen vao
         * 
         * @apiErrorExample Error-Request:
         *     HTTP/1.1 400 Bad Request
         *     {
         *       "Message": "The request is invalid."
         *     }
         *     
         * @apiError (404) NotFound id truyen vao khong ton tai
         */
        [AllowAnonymous]
        [Authorize]
        [HttpPut]
        [Route("PutBillOut")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutBillOut(int id, BillOut billOut)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!BillOutExists(id))
            {
                return NotFound();
            }
            var bo = db.BillOuts.FirstOrDefault(a => a.BillOutId == id);
            bo.EmployeeId = billOut.EmployeeId;
            bo.CustomerPhone = billOut.CustomerPhone;
            bo.BillCreateDate = billOut.BillCreateDate;
            bo.BillPriceTotal = billOut.BillPriceTotal;
            bo.PaymentMethod = billOut.PaymentMethod;
            bo.Refunds = billOut.Refunds;
            bo.AreaId = billOut.AreaId;
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        /**
         * @api {post} /BillOuts/PostBillOut Them du lieu trong hoa don
         * @apiVersion 0.1.0
         * @apiName PostBillOut
         * @apiGroup BillOuts
         * 
         * @apiHeader {int} EmployeeId Nhap vao ma cua Nhan vien
         * @apiHeader {string} CustomerPhone Nhap vao sdt cua khach hang
         * @apiHeader {DateTime} BillCreateDate Nhap vao ngay tao hoa don
         * @apiHeader {double} BillPriceTotal Nhap vao tong so tien cua hoa don
         * @apiHeader {string} PaymentMethod Nhap vao phuong thuc tra tien
         * @apiHeader {double} Refunds Nhap vao so tien tra lai
         * @apiHeader {int} AreaId Nhap vao ma chi nhanh
         * 
         * @apiHeaderExample {json} Header-Example:
         *      
         *       {
         *           "EmployeeId": 1,
         *           "CustomerPhone": null,
         *           "BillCreateDate": "2018/12/14",
         *           "BillPriceTotal": 20000,
         *           "PaymentMethod": null,
         *           "Refunds": null,
         *           "AreaId": null,
         *           "Id": "1",
         *       }
         *      
         * @apiError (400) BadRequest khi khong co gia tri nhap vao
         * 
         * @apiErrorExample Error-Request:
         *     HTTP/1.1 400 Bad Request
         *     {
         *       "Message": "The request is invalid."
         *     }
         * 
         * @apiSuccess {int} BillOutId Ma cua hoa don
         * @apiSuccess {int} EmployeeId ma cua Nhan vien
         * @apiSuccess {string} CustomerPhone sdt cua khach hang
         * @apiSuccess {DateTime} BillCreateDate ngay tao hoa don
         * @apiSuccess {double} BillPriceTotal tong so tien cua hoa don
         * @apiSuccess {string} PaymentMethod phuong thuc tra tien
         * @apiSuccess {double} Refunds so tien tra lai
         * @apiSuccess {int} AreaId ma chi nhanh
         * 
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *     {
         *           "BillOutId": 6,
         *           "BillOutDetails": null,
         *           "EmployeeId": 1,
         *           "Employees": null,
         *           "CustomerPhone": null,
         *           "Customers": null,
         *           "BillCreateDate": "2018-12-14T00:00:00",
         *           "BillPriceTotal": 20000,
         *           "PaymentMethod": null,
         *           "Refunds": null,
         *           "AreaId": null,
         *           "Areas": null,
         *           "Id": "1",
         *           "ApplicationUsers": null
         *     }
         * 
         */
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("PostBillOut")]
        [ResponseType(typeof(BillOut))]
        public IHttpActionResult PostBillOut(BillOut billOut)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.BillOuts.Add(billOut);
            db.SaveChanges();

            return Ok(billOut);
        }

        //[HttpDelete]
        //[Route("DeleteBillOut")]
        //[ResponseType(typeof(BillOut))]
        //public IHttpActionResult DeleteBillOut(int id)
        //{
        //    BillOut billOut = db.BillOuts.Find(id);
        //    if (billOut == null)
        //    {
        //        return NotFound();
        //    }

        //    db.BillOuts.Remove(billOut);
        //    db.SaveChanges();

        //    return Ok(billOut);
        //}

        //[HttpGet]
        //[Route("GetBillOuts")]
        //public IQueryable<BillOut> GetBillOuts()
        //{
        //    return db.BillOuts;
        //}

        //[HttpGet]
        //[Route("GetBillOut")]
        //[ResponseType(typeof(BillOut))]
        //public IHttpActionResult GetBillOut(int id)
        //{
        //    BillOut billOut = db.BillOuts.Find(id);
        //    if (billOut == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(billOut);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BillOutExists(int id)
        {
            return db.BillOuts.Count(e => e.BillOutId == id) > 0;
        }
    }
}