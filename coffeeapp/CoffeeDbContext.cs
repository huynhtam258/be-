﻿    using coffeeapp.Models.dbcoffee;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace coffeeapp
{
    public class CoffeeDbContext : DbContext
    {
        public CoffeeDbContext() : base("CoffeeAppConnection")
        {
            //khi load bang cha khong tu dong include bang con
            this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Area> Areas { set; get; }
        public DbSet<BillOut> BillOuts { set; get; }
        public DbSet<BillOutDetail> BillOutDetails { set; get; }
        public DbSet<Customer> Customers { set; get; }
        public DbSet<Employee> Employees { set; get; }
        public DbSet<Position> Positions { set; get; }
        public DbSet<Product> Products { set; get; }
        public DbSet<Rank> Ranks { set; get; }
        public DbSet<Stock> Stocks { set; get; }
        public DbSet<Table> Tables { set; get; }
        public DbSet<TypeProduct> TypeProducts { set; get; }

        public static CoffeeDbContext Create()
        {
            return new CoffeeDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
        }
        
    }
}